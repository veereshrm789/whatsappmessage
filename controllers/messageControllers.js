const { getTextMessageInput, sendMessage } = require("../message/messageHelper");

const welcomeMessage = async (ree, res, next) => {
  const data = getTextMessageInput(
    process.env.RECIPIENT_WAID,
    "Welcome to the Movie Ticket Demo App for Node.js!"
  );

  sendMessage(data)
    .then(function (response) {
      res.sendStatus(200);
      return;
    })
    .catch(function (error) {
      console.log(error);
      console.log(error.response.data);
      res.sendStatus(500);
      return;
    });
};

module.exports = { welcomeMessage };
