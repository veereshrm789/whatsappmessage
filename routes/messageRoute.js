const { welcomeMessage } = require("../controllers/messageControllers");

const router = require("express").Router();

router.route("/").post(welcomeMessage);

module.exports = router;
