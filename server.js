const express = require("express");
const dotenv = require("dotenv");
const messageRoute = require("./routes/messageRoute");

const app = express();

dotenv.config();
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Welcome to whats app automated messaging");
});

app.use("/whatsappautomated/api/message", messageRoute);

const PORT = process.env.RUN_PORT || 5000;

app.listen(PORT, () => {
  console.log(` Server started on PORT ${PORT}`);
});
